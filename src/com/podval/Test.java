package com.podval;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        int[][] Edges = new int[][]{
                {0, 1},
                {1, 2},
                {1, 3},
                {1, 4},
                {2, 4},
                {6, 7},
                {6, 8},
                {7, 8}
        };
        int vertexCount = 9;
        int[] comp;

        ConnectivityComponentsAlgs naive = new NaiveAlgorithm(Edges, vertexCount);
        ConnectivityComponentsAlgs rem = new RemAlgorithm(Edges, vertexCount);

        naive.solveAndPrint();
        rem.solveAndPrint();
    }
}

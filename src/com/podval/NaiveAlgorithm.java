package com.podval;

import java.util.Arrays;

public class NaiveAlgorithm implements ConnectivityComponentsAlgs {
    private int[][] edges;
    private int[] components = null;
    private int edgeCount;
    private int vertexCount;

    NaiveAlgorithm(int[][] edges, int vertexCount){
        this.vertexCount = vertexCount;

        this.edgeCount = edges.length;
        this.edges = new int[edgeCount][2];

        for(int i = 0; i < edgeCount; i++){
            this.edges[i][0] = edges[i][0];
            this.edges[i][1] = edges[i][1];
        }

        components = new int[vertexCount];
    }

    @Override
    public int[] solve() {

        for (int i = 0; i < vertexCount; i++){
            components[i] = i;
        }

        for (int i = 0; i < vertexCount - 1; i++){
            for(int j = 0; j < edgeCount; j++){

                int q = Math.min(components[edges[j][0]], components[edges[j][1]]);

                components[edges[j][0]] = q;
                components[edges[j][1]] = q;

            }
        }

        return components.clone();
    }

    @Override
    public void printSolution() {
        System.out.println(Arrays.toString(components));
    }

    @Override
    public int[] solveAndPrint() {
        int[] result = solve();

        System.out.println(Arrays.toString(components));
        return result;
    }
}

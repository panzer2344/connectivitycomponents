package com.podval;

import java.util.Arrays;

public class RemAlgorithm implements ConnectivityComponentsAlgs {
    private int[][] edges;
    private int[] components = null;
    private int edgeCount;
    private int vertexCount;
    private SplitSets splitSets;

    private class SplitSets{
        private int[] marks = null;
        private int marksCount;

        SplitSets(int setsCount){
            marksCount = setsCount;
            marks = new int[marksCount];

            for(int i = 0; i < marksCount; i++){
                marks[i] = i;
            }
        }

        public void makeSet(int a, int i){
            marks[i] = a;
        }

        public int findSet(int i){
            return marks[i];
        }

        public void union(int x, int y){
            for(int i = 0; i < marksCount; i++){
                if(marks[i] == x){
                    marks[i] = y;
                }
            }
        }
    }

    RemAlgorithm(int[][] edges, int vertexCount){
        this.vertexCount = vertexCount;

        this.edgeCount = edges.length;
        this.edges = new int[edgeCount][2];

        for(int i = 0; i < edgeCount; i++){
            this.edges[i][0] = edges[i][0];
            this.edges[i][1] = edges[i][1];
        }

        components = new int[vertexCount];
        splitSets = new SplitSets(vertexCount);
    }

    private void createSets(){
        for(int i = 0; i < vertexCount; i++){
            splitSets.makeSet(i, i);
        }
    }

    @Override
    public int[] solve() {

        createSets();

        for(int i = 0; i < edgeCount; i++){
            int n1 = splitSets.findSet(edges[i][0]);
            int n2 = splitSets.findSet(edges[i][1]);

            if(n1 != n2){
                splitSets.union(n2, n1);
            }
        }
        for(int i = 0; i < vertexCount; i++){
            components[i] = splitSets.findSet(i);
        }

        return components.clone();
    }

    @Override
    public void printSolution() {
        System.out.println(Arrays.toString(components));
    }

    @Override
    public int[] solveAndPrint() {
        int[] result = solve();

        System.out.println(Arrays.toString(components));
        return result;
    }
}

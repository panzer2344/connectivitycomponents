package com.podval;

import java.lang.reflect.Array;

public interface ConnectivityComponentsAlgs {
    int[] solve();
    void printSolution();
    int[] solveAndPrint();
}
